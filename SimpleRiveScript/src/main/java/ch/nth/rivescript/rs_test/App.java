package ch.nth.rivescript.rs_test;

import com.rivescript.RiveScript;

import ch.nth.rivescript.rs_test.macros.AddItem;
import ch.nth.rivescript.rs_test.macros.EndConversation;
import ch.nth.rivescript.rs_test.macros.GetOrder;
import ch.nth.rivescript.rs_test.models.Menu;
import ch.nth.rivescript.rs_test.models.Order;

/**
 * Hello world!
 *
 */
public class App extends CustomShell {

	@Override
	protected void init(RiveScript bot) {
		bot.setSubroutine("addItem", new AddItem());
		bot.setSubroutine("getOrder", new GetOrder());
		bot.setSubroutine("endConversation", new EndConversation());
	}

	public static void main(String[] args) {
		order = new Order();
		menu = new Menu();
		menu.initMenuItems();

		if (args.length == 0) {
			String path = App.class.getClassLoader().getResource("pizza").getFile();
			args = new String[] { path };
		}
		new App().run(args);
	}

}

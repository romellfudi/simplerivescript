package ch.nth.rivescript.rs_test;

public enum MenuCategory {
	PIZZAS("Pizzas"),
	EXTRAS("Extras");

	private String category;

	MenuCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}
}

package ch.nth.rivescript.rs_test.macros;

import com.rivescript.RiveScript;
import com.rivescript.macro.Subroutine;

import ch.nth.rivescript.rs_test.App;

public class GetOrder implements Subroutine {

	@Override
	public String call(RiveScript rs, String[] args) {
		return App.order.toString();
	}

}
